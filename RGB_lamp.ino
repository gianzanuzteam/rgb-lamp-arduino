#include <EEPROM.h>

#define RED     6
#define GREEN   5
#define BLUE    3

//#define BLUETOOTH_CONFIG
//#define HALF_SCALE
#define EEPROM_SAVE_TIMER   60

static char serialBuffer[200];
static char* serialBufferPtr;
static bool serialData = false;

static uint8_t red   = 127;
static uint8_t green = 127;
static uint8_t blue  = 127;
static unsigned long currentTime = 0;
static bool valueChanged = false;

bool EEPROM_write_config(void);
bool EEPROM_read_config(void);

/* Valores Iniciais */
void setup() 
{
  pinMode(RED, OUTPUT);
  pinMode(GREEN, OUTPUT);
  pinMode(BLUE, OUTPUT);

  /* Obtém configuração salva na EEPROM, caso haja */
  EEPROM_read_config();

  /* Escreve valor nos LEDs */
  analogWrite(RED, red);
  analogWrite(GREEN, green);
  analogWrite(BLUE, blue);

  /* Inicializa conexão Serial-Bluetooth */
  serialBufferPtr = serialBuffer;
  memset(serialBuffer, 0, sizeof(serialBuffer));
#ifdef BLUETOOTH_CONFIG
  /* Configura baudrate do bluetooth */
  Serial.begin(9600);         delay(100);
  Serial.write("AT");         delay(100);
  Serial.write("AT+BAUD8");   delay(500);
  Serial.end();
  memset(serialBuffer, 0, sizeof(serialBuffer));
  
  /* Envia nome ao bluetooth e ignora confirmação */
  Serial.begin(115200);       delay(100);
  Serial.write("AT");         delay(100);
  Serial.write("AT+NAMERGB_Lamp003"); delay(500);
  memset(serialBuffer, 0, sizeof(serialBuffer));
#else
  Serial.begin(115200);       
  delay(100);
#endif
}

void loop() 
{
  /* Verificação periódica para salvar cor atual na EEPROM */
  if((millis() - currentTime) >  (unsigned long) EEPROM_SAVE_TIMER*1000)
  {
    /* Verifica se houve mudança no valor da cor */
    /* Salva na EEPROM caso positivo */
    /* Atualiza valor do timer */
    if(valueChanged)
      EEPROM_write_config();
    valueChanged = false;
    currentTime = millis();
  }
  
  /* Aplica comando recebido */
  if(serialData)
  {
    char* tkn = strtok(serialBuffer, "\n");
    if(!strcmp(tkn, "#SET_COLOUR"))
    {
        /* Vermelho */
        tkn = strtok(NULL, "\n");
        red = (uint8_t) atoi(tkn);
#ifdef HALF_SCALE
        /* Utiliza apenas metade da escala do PWM */
        red = (red >> 1) | (1 << 7);
#endif
        analogWrite(RED, red);

        /* Verde */
        tkn = strtok(NULL, "\n");
        green = (uint8_t) atoi(tkn);
#ifdef HALF_SCALE
        /* Utiliza apenas metade da escala do PWM */
        green = (green >> 1) | (1 << 7);
#endif
        analogWrite(GREEN, green);

        /* Azul */
        tkn = strtok(NULL, "\n");
        blue = (uint8_t) atoi(tkn);
#ifdef HALF_SCALE
        /* Utiliza apenas metade da escala do PWM */
        blue = (blue >> 1) | (1 << 7);
#endif
        analogWrite(BLUE, blue);

        /* END */
        tkn = strtok(NULL, "\n");

        /* Indica que houve mudança na cor */
        valueChanged = true;
    }

    memset(serialBuffer, 0, sizeof(serialBuffer));
    serialBufferPtr = serialBuffer;
    serialData = false;
  }
}

void serialEvent()
{
  /* Obtém bytes disponíveis */
  while (Serial.available()) 
  {
    char inChar = (char)Serial.read();
    *serialBufferPtr = inChar;  serialBufferPtr++;
    *serialBufferPtr = '\0';
  }
  /* Procura pelo final do comando */
  if(strstr(serialBuffer, "#END\n"))
    serialData = true;
}

/************************************************************************************
* EEPROM_write_config
*
* Writes the current color in EEPROM
*
************************************************************************************/
bool EEPROM_write_config(void)
{
    uint16_t addr=0;

    /* Indicador de início */
    EEPROM.write(addr, 0xFE);
    addr++;
    
    /* red */
    EEPROM.write(addr, red);
    addr++;
    /* green */
    EEPROM.write(addr, green);
    addr++;
    /* blue */
    EEPROM.write(addr, blue);
    addr++;

    return true;
}

/************************************************************************************
* EEPROM_read_config
*
* Reads the previous color from EEPROM
*
************************************************************************************/
bool EEPROM_read_config(void)
{
    uint16_t addr=0;

    /* Indicador de início */
    if(EEPROM.read(addr) != 0xFE)
        return false;
    addr++;

    /* red */
    red = EEPROM.read(addr);
    addr++;
    /* green */
    green = EEPROM.read(addr);
    addr++;
    /* blue */
    blue = EEPROM.read(addr);
    addr++;

    return true;
}
